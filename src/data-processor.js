const _ = require('lodash');
const providers = require('./data-providers');
const Emitter = require('./emitter');

const emitter = new Emitter();
let coinMarketPrice = 0;
let cexIOPrice = 0;
let lastDelta = 0;


function getDeltaPercent(cex, coinMarket) {
  let result = 0;
  if (_.isNumber(cex) && _.isNumber(coinMarket) && cex !== 0 && coinMarket !== 0) {
    result = ((cex - coinMarket) / coinMarket) * 100;
  }
  return result;
}

function onData(data) {
  let delta;
  switch (data.name) {
    case 'CoinMarketCap':
      coinMarketPrice = data.price;
      break;
    case 'CexIO':
      cexIOPrice = data.price;
      delta = getDeltaPercent(cexIOPrice, coinMarketPrice);
      if (delta !== lastDelta) {
        emitter.emit(delta);
        lastDelta = delta;
      }
      break;
    default:
  }
}


// set callback for providers
for (let i = 0, l = providers.length; i < l; ++i) {
  providers[i].on(onData);
}


exports.onDelta = (listener) => {
  emitter.add(listener);
};

if (process.env.TEST !== undefined) {
  exports.__test__ = { //eslint-disable-line
    getDeltaPercent,
    onData,
  };
}
