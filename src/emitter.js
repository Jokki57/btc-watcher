const _ = require('lodash');

// this is no actually EventEmitter, just emitter for one event without type %)
module.exports = class {
  constructor() {
    this.listeners = [];
  }

  add(listener) {
    if (_.isFunction(listener)) {
      this.listeners.push(listener);
    }
  }

  remove(listener) {
    const index = this.listeners.indexOf(listener);
    if (index !== -1) {
      this.listeners.splice(index, 1);
    }
  }

  emit(data) {
    for (let i = 0, l = this.listeners.length; i < l; ++i) {
      this.listeners[i](data);
    }
  }

  get listenersCout() {
    return this.listeners.length;
  }
};
