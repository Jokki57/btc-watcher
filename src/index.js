const dataProcessor = require('./data-processor');
const logger = require('./logger');

logger.init(console);

dataProcessor.onDelta((delta, error) => {
  if (error) {
    logger.error(error);
  } else {
    logger.printPrice(new Date(), delta);
  }
});
