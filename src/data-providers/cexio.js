const crypto = require('crypto');
const WebSocket = require('ws');
const _ = require('lodash');
const Emitter = require('../emitter');
const logger = require('../logger');

const NAME = 'CexIO';
const ws = new WebSocket('wss://ws.cex.io/ws/');
const emitter = new Emitter();

function createSignature(timestamp, apiKey, apiSecret) {
  const hmac = crypto.createHmac('sha256', apiSecret);
  hmac.update(timestamp + apiKey);
  return hmac.digest('hex');
}

function createAuthRequest(timestamp, apiKey, apiSecret) {
  const args = {
    e: 'auth',
    auth: {
      key: apiKey,
      signature: createSignature(timestamp, apiKey, apiSecret),
      timestamp,
    },
  };
  return JSON.stringify(args);
}


function onSocketMessage(message) {
  let data;
  try {
    data = JSON.parse(message);
  } catch (error) {
    data = null;
  }
  if (data && data.ok === 'ok') {
    let answer;
    switch (data.e) {
      case 'ping':
        answer = { e: 'pong' };
        break;
      case 'auth':
        answer = {
          e: 'subscribe',
          rooms: [
            'tickers',
          ],
        };
        break;
      case 'tick':
        if (_.get(data, 'data.symbol1') === 'BTC' && _.get(data, 'data.symbol2') === 'USD') {
          const price = Number(_.get(data, 'data.price'));
          if (!Number.isNaN(price)) {
            emitter.emit({
              name: NAME,
              price,
            });
          }
        }
        break;
      default:
    }
    if (answer) {
      answer = JSON.stringify(answer);
      ws.send(answer);
    }
  } else if (data && data.ok === 'error') {
    logger.error(`CEXIO; ${data}`);
  }
}

function onOpen() {
  ws.send(createAuthRequest(Math.floor(Date.now() / 1000), 'Ih2MkynvDFddPcJJKTAK15kbBM', 'hmzTCJ7Ce9XLGBUienrhh0HlQQg'));
  ws.on('message', onSocketMessage);
}

ws.on('open', onOpen);


exports.on = (listener) => {
  emitter.add(listener);
};

exports.off = (listener) => {
  emitter.remove(listener);
};


if (process.env.TEST !== undefined) {
  exports.__test__ = { //eslint-disable-line
    createSignature,
    createAuthRequest,
    onSocketMessage,
    onOpen,
  };
}
