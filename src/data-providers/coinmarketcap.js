const axios = require('axios');
const _ = require('lodash');
const Emitter = require('../emitter');
const config = require('../config');
const logger = require('../logger');

const INTERVAL = config.coinmarketUpdate || 10000;
const NAME = 'CoinMarketCap';
const emitter = new Emitter();

let intervalId = null;

function fetchData() {
  axios.get('https://api.coinmarketcap.com/v1/ticker/bitcoin/')
    .then((response) => {
      const data = Number(_.get(response.data, '0.price_usd'));
      if (!Number.isNaN(data)) {
        emitter.emit({
          name: NAME,
          price: data,
        });
      }
    })
    .catch((error) => {
      logger.error(`CoinMarket; ${error.toString()}`);
    });
}

function startFetchData() {
  intervalId = setInterval(fetchData, INTERVAL);
}

function stopFetchData() {
  if (intervalId) {
    clearInterval(intervalId);
  }
}


exports.on = (listener) => {
  emitter.add(listener);
  if (emitter.listenersCout === 1) {
    startFetchData();
  }
};

exports.off = (listener) => {
  emitter.remove(listener);
  if (emitter.listenersCout === 0) {
    stopFetchData();
  }
};

if (process.env.TEST !== undefined) {
  exports.__test__ = { //eslint-disable-line
    fetchData,
    startFetchData,
    stopFetchData,
  };
}

