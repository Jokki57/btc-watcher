const cexio = require('./cexio');
const coinmarketcap = require('./coinmarketcap');

module.exports = [cexio, coinmarketcap];
