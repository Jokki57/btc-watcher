const _ = require('lodash');
const colors = require('colors/safe');
const config = require('./config');
const numeral = require('numeral');
const dateformat = require('dateformat');

const timeTemplate = config.timeTemplate || 'h:MM:ss';
const signCount = config.signCount || 4;
const printDelimiter = config.printDelimiter || ' : ';

let output = null;
let numeralFormat = '0.';
for (let i = 0; i < signCount; ++i) {
  numeralFormat += '0';
}
numeralFormat += '%';


const init = (output_) => {
  if (output_ && _.isFunction(output_.log)) {
    output = output_;
  }
};

const log = (...args) => {
  if (output) {
    output.log(...args);
  }
};

const printPrice = (date, deltaPrice) => {
  const priceString = deltaPrice > 0
    ? colors.green(numeral(deltaPrice).format(numeralFormat))
    : colors.red(numeral(deltaPrice).format(numeralFormat));
  const dateString = dateformat(date, timeTemplate);
  log(`${colors.grey(dateString)}${printDelimiter}${priceString}`);
};

const error = (message) => {
  log(colors.red(`Error: ${dateformat(new Date(), timeTemplate)}${printDelimiter}${message}`));
};

exports.init = init;
exports.log = log;
exports.printPrice = printPrice;
exports.error = error;
