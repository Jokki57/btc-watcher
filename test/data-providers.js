// Написал тесты лишь для одного модуля,
// так как их написание занимает продолжительное время

/* eslint-disable no-underscore-dangle, class-methods-use-this */
const proxyquire = require('proxyquire');
const assert = require('assert');

describe('Data Providers', () => {
  describe('cexio', () => {
    let socketResult;
    let emitterResult;
    let cexio;

    class WebSocket {
      constructor(url) {
        socketResult.url = url;
      }

      on(type) {
        socketResult.type = type;
      }

      send(answer) {
        socketResult.answer = answer;
      }
    }

    class Emitter {
      emit(data) {
        emitterResult.data = data;
      }
    };

    beforeEach(() => {
      socketResult = {};
      emitterResult = {};
      cexio = proxyquire('../src/data-providers/cexio', { ws: WebSocket, '../emitter': Emitter });
    });

    it('createSignature', () => {
      const { createSignature } = cexio.__test__;
      const timestamp = 1521999257166;
      const result = createSignature(timestamp, 'asd', 'dsa');
      assert.equal(result, '9bb741ae1faa6095dac866180f2b8a17e8d4d7eb50da408c7f0a643918192920');
    });

    it('createAuthRequest', () => {
      const { createAuthRequest } = cexio.__test__;
      const timestamp = 1521999257166;
      const result = createAuthRequest(timestamp, 'asd', 'dsa');

      assert.equal(result, JSON.stringify({
        e: 'auth',
        auth: {
          key: 'asd',
          signature: '9bb741ae1faa6095dac866180f2b8a17e8d4d7eb50da408c7f0a643918192920',
          timestamp,
        },
      }));
    });

    it('websocket url', () => {
      assert.equal(socketResult.url, 'wss://ws.cex.io/ws/');
    });


    it('onOpen', () => {
      const { onOpen } = cexio.__test__;
      onOpen();
      const answer = JSON.parse(socketResult.answer);
      assert.equal(answer.e, 'auth');
      if ('auth' in answer) {
        const { auth } = answer;
        if (!('key' in auth)) {
          throw new Error('ke field is absent');
        } else if (typeof auth.key !== 'string') {
          throw new Error('key field is absent');
        }

        if (!('signature' in auth)) {
          throw new Error('signature field is absent');
        } else if (typeof auth.signature !== 'string') {
          throw new Error('signature must be string');
        }

        if (!('timestamp' in auth)) {
          throw new Error('auth field is absent');
        } else if (typeof auth.timestamp !== 'number') {
          throw new Error('timestamp must be number');
        }
      } else {
        throw new Error('auth field is absent');
      }
    });


    describe('onSocketMessage', () => {
      it('ping', () => {
        const { onSocketMessage } = cexio.__test__;
        onSocketMessage(JSON.stringify({ e: 'ping', ok: 'ok' }));
        assert.equal(socketResult.answer, JSON.stringify({ e: 'pong' }));
      });

      it('auth', () => {
        const { onSocketMessage } = cexio.__test__;
        onSocketMessage(JSON.stringify({ e: 'auth', ok: 'ok' }));
        assert.equal(socketResult.answer, JSON.stringify({
          e: 'subscribe',
          rooms: [
            'tickers',
          ],
        }));
      });

      it('tick', () => {
        const { onSocketMessage } = cexio.__test__;
        onSocketMessage(JSON.stringify({
          e: 'tick',
          ok: 'ok',
          data: {
            symbol1: 'BTC',
            symbol2: 'USD',
            price: 123,
          },
        }));
        assert.deepEqual(emitterResult.data, {
          name: 'CexIO',
          price: 123,
        });
      });

      it('bad message', () => {
        const { onSocketMessage } = cexio.__test__;
        onSocketMessage('');
        assert.deepEqual(emitterResult, {});
        assert.deepEqual(socketResult, { url: 'wss://ws.cex.io/ws/', type: 'open' });
      });
    });
  });
});
